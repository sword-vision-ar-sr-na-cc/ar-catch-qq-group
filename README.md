# ArCatch-QQ Group

#### 介绍
利用原生JavaScript与QQ网页管理获取全群成员QQ信息
没什么用，但还是挺有用的

#### 软件架构
软件架构说明：原生JavaScript，直接可以在浏览器运行


#### 安装教程

1.  打开 [QQ群管理](https://qun.qq.com/member.html) 选择群，然后滑到底保证全部加载完成
2.  F12打开控制台，将index.js粘贴到控制台或直接拖入
3.  完成

#### 使用说明

方法：ArCatch.method()
1.  获取群员QQ昵称：ArCatch.nickName()，将会在控制台输出昵称
2.  获取群员群名片：ArCatch.groupName()或ArCatch.groupNameAll()，将会在控制台输出昵称(0)
3.  获取QQ：ArCatch.QQ()，将会在控制台输出(1)
4.  DEMO：ArCatch.all() (2)

(0)：如果部分群名片是空白的（即默认的），如果是空的将返回空值，用ArCatch.groupNameAll()方法只能一条条输出，其他全部将字符串压缩自动换行，输出一次
(1)：由于没法通过td标签获取qq号，所以只能通过className(mb)然后截取mb mb字符串即可获得qq
(2)：根据群名片进行，由于部分群名片是空白的（即默认的），所以加入了判断，如果是空则获取qq昵称

## 此致：Ar-Sr-Na 云计算团队SwordVision


#### 参与贡献

1.  Fork 本仓库
2.  新建 {id} 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
